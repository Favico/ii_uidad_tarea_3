import pygame

FONDO = (255,255,255)
(width, height) = (800, 600)
Negro=(0,0,0)
Rojo = (255, 0, 0)
Cafe = (90, 50, 15)

Pantalla = pygame.display.set_mode((width, height))
pygame.display.set_caption('Grafico del  Plano Cartesiano')
Pantalla.fill(FONDO)


pygame.draw.line(Pantalla,Negro, (400,0), (400,600),3)
pygame.draw.line(Pantalla,Negro, (0,300), (800,300),3)
pygame.draw.circle(Pantalla,Rojo, (400,300),(5),0)
#Eje de las X

pygame.draw.line(Pantalla,Negro, (50,290), (50,310),3)
pygame.draw.line(Pantalla,Negro, (100,290), (100,310),3)
pygame.draw.line(Pantalla,Negro, (150,290), (150,310),3)
pygame.draw.line(Pantalla,Negro, (200,290), (200,310),3)
pygame.draw.line(Pantalla,Negro, (250,290), (250,310),3)
pygame.draw.line(Pantalla,Negro, (300,290), (300,310),3)
pygame.draw.line(Pantalla,Negro, (350,290), (350,310),3)
pygame.draw.line(Pantalla,Negro, (450,290), (450,310),3)
pygame.draw.line(Pantalla,Negro, (500,290), (500,310),3)
pygame.draw.line(Pantalla,Negro, (550,290), (550,310),3)
pygame.draw.line(Pantalla,Negro, (600,290), (600,310),3)
pygame.draw.line(Pantalla,Negro, (650,290), (650,310),3)
pygame.draw.line(Pantalla,Negro, (700,290), (700,310),3)
pygame.draw.line(Pantalla,Negro, (750,290), (750,310),3)
#Eje de las Y
pygame.draw.line(Pantalla,Negro, (390,50), (410,50),3)
pygame.draw.line(Pantalla,Negro, (390,100), (410,100),3)
pygame.draw.line(Pantalla,Negro, (390,150), (410,150),3)
pygame.draw.line(Pantalla,Negro, (390,200), (410,200),3)
pygame.draw.line(Pantalla,Negro, (390,250), (410,250),3)
pygame.draw.line(Pantalla,Negro, (390,350), (410,350),3)
pygame.draw.line(Pantalla,Negro, (390,400), (410,400),3)
pygame.draw.line(Pantalla,Negro, (390,450), (410,450),3)
pygame.draw.line(Pantalla,Negro, (390,500), (410,500),3)
pygame.draw.line(Pantalla,Negro, (390,550), (410,550),3)

#Cuadrado
pygame.draw.rect(Pantalla,Cafe,(500,150, 150, -100))
#Punto A
pygame.draw.circle(Pantalla,Rojo, (500,150),(5),0)
#Punto B
pygame.draw.circle(Pantalla,Rojo, (650,50),(5),0)
#Punto C
pygame.draw.circle(Pantalla,Rojo, (250,350),(5),0)
#Punto D
pygame.draw.circle(Pantalla,Rojo, (400,300),(5),0)


Terminar = False
reloj = pygame.time.Clock()

while not Terminar:
     for Evento in pygame.event.get():
        if Evento.type == pygame.QUIT:
            Terminar = True



     pygame.display.flip()
     reloj.tick(20)
pygame.quit()