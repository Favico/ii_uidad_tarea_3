"""Tarea 3
CREE  UN MÓDULO PYTHON  LLAMADO "geometria" Y EN EL REALICE LO SIGUIENTE:

Crear una clase llamada Punto con sus dos coordenadas X e Y.
Añadir un método constructor para crear puntos fácilmente. Si no se reciben una coordenada, su valor será cero.
Sobreescribir el método __str__, para que al imprimir por pantalla un punto aparezca en formato (X,Y)
Añadir un método llamado cuadrante que indique a qué cuadrante pertenece el punto, teniendo en cuenta que si X == 0 e Y != 0 se sitúa sobre el eje Y, si X != 0 e Y == 0 se sitúa sobre el eje X y si X == 0 e Y == 0 está sobre el origen.
Añadir un método llamado vector, que tome otro punto como parámetro y calcule el vector resultante entre los dos puntos.
Añadr un método llamado distancia, que tome otro punto como parámetro y calcule la distancia entre los dos puntos y la devuelva.
Crear una clase llamada Rectangulo con dos puntos (inicial y final) que formarán la diagonal del rectángulo.
Añadir un método constructor para crear ambos puntos fácilmente, si no se envían se crearán dos puntos en el origen por defecto.
Añadir al rectángulo un método llamado base que devuelva la base.
Añade al rectángulo un método llamado altura que devuelva la altura.
Añade al rectángulo un método llamado area que devuelva el area."""

import math
class Punto:
    x = int
    y = int

    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


    def __str__(self):
        return "({}, {})".format(self.x, self.y)


    def cuadrante(self):
        if self.x > 0 and self.y > 0:
            print("A = {} Pertenece al primer cuadrante".format(self))
        elif self.x < 0 and self.y > 0:
            print(" B = {} pertenece al segundo cuadrante".format(self))
        elif self.x < 0 and self.y < 0:
            print("C = {} pertenece al tercer cuadrante".format(self))
        elif self.x == 0 and self.y == 0:
            print("D = {} pertenece al cuarto cuadrante\n".format(self))
        elif self.x != 0 and self.y == 0:
            print("{} se sitúa sobre el eje X".format(self))
        elif self.x == 0 and self.y != 0:
            print("{} se sitúa sobre el eje Y".format(self))
        else:
            print("{} se encuentra sobre el origen".format(self))

    def vector(self, p):
            self, p, p.x - self.x, p.y - self.y

    def distancia(self, p):
        d = math.sqrt( (p.x - self.x)**2 + (p.y - self.y)**2 )
        print("La distancia entre los puntos {} y {} es {}".format(
            self, p, d))


class Rectangulo:

    def __init__(self, punto_inicial=Punto(), punto_final=Punto()):
        self.pInicial = punto_inicial
        self.pFinal = punto_final

        self.Base = abs(self.pFinal.x - self.pInicial.x)
        self.Altura = abs(self.pFinal.y - self.pInicial.y)
        self.Area = self.Base * self.Altura

    def base(self):
        print("\nLa base del rectángulo es {}".format( self.Base))

    def altura(self):
        print("La altura del rectángulo es {}".format( self.Altura))

    def area(self):
        print("El área del rectángulo es {}".format( self.Area))


A = Punto(2, 3)
B = Punto(5, 5)
C = Punto(-3, -1)
D = Punto(0, 0)
print(f'A = {A}')
print(f'B = {B}')
print(f'C = {C}')
print(f'D = {D}\n')

A.cuadrante()
C.cuadrante()
D.cuadrante()

A.vector(B)
B.vector(A)

A.distancia(B)
B.distancia(A)

A.distancia(D)
B.distancia(D)
C.distancia(D)

R = Rectangulo(A, B)
R.base()
R.altura()
R.area()
